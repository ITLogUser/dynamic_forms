import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TestFormService } from './test-form.service';
import { BaseForm, FieldBase } from './base-form';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-form-generator',
  templateUrl: './form-generator.component.html',
  styleUrls: ['./form-generator.component.scss']
})
export class FormGeneratorComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  fields: FieldBase<any>[] | null | undefined = null;
  @Input() formID: number | string | null | undefined = null; // с сервера приходит массив форм, где каждая форма имеет свой ID,
  // соответственно, в этом поле задаём ID формы, которую мы хотим отрендерить.
  subject = new Subject(); // будет эмитит ошибку, если форма не валидна на момент когда юзер нажимает 'confirm'.
  // Идея в том, дочерние инпуты будут подписываться и реагировать как-то на ошибки, например подсвечивать и т.д.

  constructor(public formService: TestFormService) { }

  ngOnInit(): void {
    // Получаем с сервера JSON файл, описывающий форму
    this.formService.getFormJSON().subscribe((resData: BaseForm[]) => {
      // todo можно сделать проверку, что пришедший с сервера JSON соответсвует формату описанному в FieldBase
      if (this.check(resData)) {
        this.fields = (resData.find(f => String(f['id']) === String(this.formID)) as BaseForm).fields;
        this.fields?.sort((a, b) => a.order - b.order);
        this.form = this.formService.toFormGroup(this.fields);
      }
    }, (error) => {
      console.error('errorMessage: ', error);
    });
  }

  // Проверяет, что пришедший с сервера JSON соответсвует формату описанному в FieldBase
  check(forms: BaseForm[] ): boolean {
    let result = true;
    if (!Array.isArray(forms)) return false;

    const currentForm = forms.find(f => String(f['id']) === String(this.formID));
    if (!currentForm) return false;

    if (currentForm.fields && currentForm.fields.every) {
      result = currentForm.fields.every(field => field.hasOwnProperty && field.hasOwnProperty('label') );
      // Еще можно много сделать проверок, но пока ограничимся проверкой наличия свойства 'label'
    } else {
      return false;
    }

    return result;
  }


  onSubmit() {
    console.log('Submit form', this.form);
    // todo сделать подсветку обязательных полей
    if (this.form.valid) {
      this.sendForm(this.form.value);
    } else {
      this.subject.next();
    }
  }

  sendForm(postData: any): void {
    this.formService.send(postData).subscribe(
      (response) => {
        console.log('form have sent', response);
      }, (error) => {
        console.error('Ошибка отправки формы', error);
      }
    );
  }
}
