import { Component, Input, OnInit } from '@angular/core';
import { FieldBase } from '../base-form';
import { FormGroup } from '@angular/forms';
import { TestFormService } from '../test-form.service';
import { Subject } from 'rxjs';
import { BaseInputHelper } from '../base-input-helper';

@Component({
  selector: 'app-test-input',
  templateUrl: './test-input.component.html',
  styleUrls: ['./test-input.component.scss']
})
export class TestInputComponent extends BaseInputHelper implements OnInit {
  @Input() form!: FormGroup;
  @Input() field: FieldBase<string> | null | undefined = null;
  @Input() subjectError!: Subject<any>;

  constructor(private formService: TestFormService) {
    super();
  }

  ngOnInit(): void {
    this.currentControl = this.formService.getFieldControl(this.form, this.field);
    this.onControlChanges(); // наследуется от BaseInputHelper
    this.subjectError.subscribe(() => { this.isValid = !!this.currentControl?.valid });
  }

}
