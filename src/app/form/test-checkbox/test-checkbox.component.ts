import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FieldBase } from '../base-form';
import { TestFormService } from '../test-form.service';
import { BaseInputHelper } from '../base-input-helper';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-test-checkbox',
  templateUrl: './test-checkbox.component.html',
  styleUrls: ['./test-checkbox.component.scss']
})
export class TestCheckboxComponent extends BaseInputHelper implements OnInit {
  @Input() form!: FormGroup;
  @Input() field: FieldBase<string> | null | undefined = null;
  @Input() subjectError!: Subject<any>;
  checkBoxData: { key: string; value: string } | null = null;
  dynamicName: string | null = null; // имя для FormControl

  constructor(private formService: TestFormService) {
    super();
  }

  ngOnInit(): void {
    if (this.field && this.field.key) {
      this.checkBoxData = { key: this.field.key, value: this.field.label };
      this.dynamicName = this.field.key;
      if (this.dynamicName) {
        this.form.setControl(this.dynamicName, this.field.required  ?
          new FormControl(null, Validators.requiredTrue) : new FormControl(null));

        this.currentControl = this.formService.getFieldControl(this.form, this.field);
        this.onControlChanges(); // наследуется от BaseInputHelper
        this.subjectError.subscribe(() => { this.isValid = !!this.currentControl?.valid });
      }
    }
  }

}
