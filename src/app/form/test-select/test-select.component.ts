import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Choice, FieldBase } from '../base-form';
import { BaseInputHelper } from '../base-input-helper';
import { TestFormService } from '../test-form.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-test-select',
  templateUrl: './test-select.component.html',
  styleUrls: ['./test-select.component.scss']
})
export class TestSelectComponent extends BaseInputHelper implements OnInit {
  @Input() form!: FormGroup;
  @Input() field: FieldBase<string> | null | undefined = null;
  @Input() subjectError!: Subject<any>;
  choices: Choice[] | null = null; // данные для тегов <option></option>
  dynamicName: string | null = null; // имя для FormControl

  constructor(private formService: TestFormService) {
    super();
  }

  ngOnInit(): void {
    if (this.field && this.field.key && this.field.choices) {
      this.choices = this.field.choices;
      this.choices = this.choices.filter((opt) => !opt.hide); // убераем поля у которых стоит hide:true
      this.dynamicName = this.field.key;
      if (this.dynamicName) {
        this.form.setControl(this.dynamicName, new FormControl(this.choices[this.findActiveIndex()], Validators.required));
        this.currentControl = this.formService.getFieldControl(this.form, this.field);
        this.onControlChanges(); // наследуется от BaseInputHelper
        this.subjectError.subscribe(() => { this.isValid = !!this.currentControl?.valid });
      }
    }
  }

  /**
   * В массиве Choice[] находит элемент у которого active:true и возвращает индекс этого элемента,
   * если не находит то возвращает ноль.
   */
  findActiveIndex(): number {
    let result = 0;
    if (this.choices) {
      this.choices.some((ch, index) => {
        if (ch.active === true) {
          result = index;
          return true;
        }
        return false;
      });
    }
    return result;
  }

}
