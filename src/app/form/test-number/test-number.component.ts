import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldBase } from '../base-form';
import { Subject } from 'rxjs';
import { BaseInputHelper } from '../base-input-helper';
import { TestFormService } from '../test-form.service';

@Component({
  selector: 'app-test-number',
  templateUrl: './test-number.component.html',
  styleUrls: ['./test-number.component.scss']
})
export class TestNumberComponent extends BaseInputHelper implements OnInit {
  @Input() form!: FormGroup;
  @Input() field: FieldBase<string> | null | undefined = null;
  @Input() subjectError!: Subject<any>;

  constructor(private formService: TestFormService) {
    super();
  }

  ngOnInit(): void {
    this.currentControl = this.formService.getFieldControl(this.form, this.field);
    this.onControlChanges(); // наследуется от BaseInputHelper
    this.subjectError.subscribe(() => { this.isValid = !!this.currentControl?.valid });
  }

}
