import { FormControl } from '@angular/forms';

/**
 * Вспомогательный класс, от которого наследуются все компоненты формы.
 * В этот класс вынесены общие методы для всех компонентов формы.
 */
export class BaseInputHelper {

  currentControl: FormControl | null = null;
  isValid = true;

  onControlChanges(): void {
    if (this.currentControl?.valueChanges.subscribe) {
      this.currentControl?.valueChanges.subscribe(() => {
        this.isValid = !!this.currentControl?.valid;
      });
    }
  }

}
