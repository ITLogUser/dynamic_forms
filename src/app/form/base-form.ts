export interface Choice {
  key: string;
  value: string;
  active?: boolean;
  hide?: boolean;
}

export class FieldBase<T> {
  id: number|string|undefined;
  value: T|undefined;
  key: string;
  label: string;
  description: string;
  required: boolean;
  order: number;
  controlType: string;
  type: string;
  choices: Choice[] | null;

  constructor(options: {
    id?: number|string;
    value?: T;
    key?: string;
    label?: string;
    description?: string;
    required?: boolean;
    order?: number;
    controlType?: string;
    type?: string;
    choices?: Choice[];
  } = {}) {
    this.id = options.id || '';
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.description = options.description || '';
    this.required = !!options.required;
    this.order = options.order === undefined ? 1 : options.order;
    this.controlType = options.controlType || '';
    this.type = options.type || '';
    this.choices = options.choices || null;
  }
}

export interface BaseForm {
  id?: string | number;
  fields?: FieldBase<any>[];
}
