import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FieldBase } from './base-form';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';

// Данный сервис отвечает за отправку формы на сервер и за получение JSON с сервера, по которому будет генерироваться форма

@Injectable({
  providedIn: 'root'
})
export class TestFormService {

  constructor( private http: HttpClient ) { }
  private endpoint = 'api';
  private formsUrl = `${this.endpoint}/forms`;  // URL to web api

  getFormJSON(): Observable<any> {
    return this.http.get<any>(this.formsUrl);
  }

  /** создает FormGroup и FormControls из полученных полей и добавляет их в FormGroup
   * Для не текстовых полей таких как <select> или <checkbox> FormControls будут переопределяться в соответствующем компоненте
   * Наприме для <select> переопределение происходит в компоненте test-select.component.ts */
  toFormGroup(fields: FieldBase<any>[] | undefined  ): FormGroup {
    const group: any = {};

    if (fields) {
      fields.forEach(field => {
        group[field.key] = field.required ? new FormControl(field.value || '', Validators.required)
          : new FormControl(field.value || '');
      });
    }
    return new FormGroup(group);
  }

  updateControlValue(formGroup: FormGroup, controlName: string, value: any = ''): void {
    const control = formGroup.controls[controlName];
    if (control) {
      control.setValue(value);
    }
  }

  getFieldControl(formGroup: FormGroup, field: FieldBase<any> | null | undefined): FormControl | null {
    if (field && field.key) {
      return formGroup.get(field.key) as FormControl;
    }
    return null;
  }

  // Отправка формы на сервер
  send(postData: any): Observable<unknown> {

    return this.http.post<unknown>(
      `${this.endpoint}/forms`,
      postData,
      {
        observe: 'body',
        /*
        body means that you get that response data extracted and converted to a JavaScript object automatically
        but you don't have to stick to body. You can for example else change this to 'response'
        observe: 'response' означает что будет возвращен полный HttpResponse object
        * */
      }
    );
  }
}
