import { Injectable } from '@angular/core';
import { InMemoryDbService, RequestInfo } from 'angular-in-memory-web-api';
import { Observable } from 'rxjs';

/*
Сервис симулирующий серверное API (Simulate a data server)
Подробнее в документации https://angular.io/tutorial/toh-pt6#simulate-a-data-server
* */

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() { }

  createDb(reqInfo?: RequestInfo): {} | Observable<{}> | Promise<{}> {
    /* Это имитация API сервера.
      Массив forms содержит объекты описывающие форму. Т.е. массив forms может содержать сколько угодно форм.
    */
    const forms = [
      {
        id: 1, // Идентификатор формы. Ну и этот id нужен для корректной работы https://github.com/angular/in-memory-web-api
        fields: [ // поля формы
          {
            id: '',
            value: '',
            key: 'key_11',
            label: 'Введите текст',
            description: 'Описание поля',
            required: true,
            order: 1, // order это порядок расположения поля внутри формы.
            controlType: 'text',
            type: 'text',
            choices: null, // это для элемента <select></select>
          },
          {
            id: '',
            value: '',
            key: 'key_12',
            label: 'Выберите автомобиль',
            description: 'Описание lorem inpsum dolor',
            required: false,
            order: 2, // order это порядок расположения поля внутри формы.
            controlType: 'dropdown',
            type: 'select',
            choices: [
              { key: 'volvo', value: 'Volvo' },
              { key: 'bmw', value: 'BMW' },
              { key: 'audi', value: 'Audi' },
              { key: 'lada', value: 'LADA', hide: true},
              { key: 'default', value: 'какой предпочитаете?', active: true},
            ],
          },
          {
            id: '',
            value: '',
            key: 'key_15',
            label: 'Выберите букву',
            description: 'Описание lorem inpsum dolor',
            required: false,
            order: 5, // order это порядок расположения поля внутри формы.
            controlType: 'dropdown',
            type: 'select',
            choices: [
              { key: 'a', value: 'Aa' },
              { key: 'b', value: 'Bb' },
              { key: 'c', value: 'Cc',},
            ],
          },
          {
            id: '',
            value: '',
            key: 'key_13',
            label: 'Введите количество',
            description: '',
            required: true,
            order: 3, // order это порядок расположения поля внутри формы.
            controlType: 'number',
            type: 'number',
            choices: null, // это для элемента <select></select>
          },
          {
            id: '',
            value: '',
            key: 'key_14_terms_accepted',
            label: 'Согласен с условиями использования сервиса',
            description: 'Описание поля про галочку',
            required: true,
            order: 4, // order это порядок расположения поля внутри формы.
            controlType: 'checkbox',
            type: 'checkbox',
            choices: null, // это для элемента <select></select>
          },
          {
            id: '',
            value: '',
            key: 'key_16_subscription',
            label: 'Поставте галочку, если хотите получать письма',
            description: 'Описание поля',
            required: false,
            order: 6, // order это порядок расположения поля внутри формы.
            controlType: 'checkbox',
            type: 'checkbox',
            choices: null, // это для элемента <select></select>
          },
          {
            id: '',
            value: '',
            key: 'key_17',
            label: 'Необязательное текстовое поле',
            description: 'Описание поля',
            required: false,
            order: 7, // order это порядок расположения поля внутри формы.
            controlType: 'text',
            type: 'text',
            choices: null, // это для элемента <select></select>
          },
        ],
      },
      {
        id: 2,
        fields: [ // поля формы
          {
            id: '',
            value: '',
            key: 'key_21',
            label: 'Емейл',
            description: 'Введите емейл',
            required: true,
            order: 1, // order это порядок расположения поля внутри формы.
            controlType: 'text',
            type: 'text',
            choices: null, // это для элемента <select></select>
          },
          {
            id: '',
            value: '',
            key: 'key_22',
            label: 'ФИО',
            description: 'Введите инициалы',
            required: true,
            order: 2, // order это порядок расположения поля внутри формы.
            controlType: 'text',
            type: 'text',
            choices: null, // это для элемента <select></select>
          },
          {
            id: '',
            value: '',
            key: 'key_23',
            label: 'Пароль',
            description: 'Придумайте надёжный пароль',
            required: true,
            order: 3, // order это порядок расположения поля внутри формы.
            controlType: 'text',
            type: 'text',
            choices: null, // это для элемента <select></select>
          },
          {
            id: '',
            value: '',
            key: 'key_24',
            label: 'Введите пароль еще раз',
            description: '',
            required: true,
            order: 4, // order это порядок расположения поля внутри формы.
            controlType: 'text',
            type: 'text',
            choices: null, // это для элемента <select></select>
          },
          {
            id: '',
            value: '',
            key: 'key_25',
            label: 'О себе',
            description: 'Короткий рассказ о себе',
            required: false,
            order: 5, // order это порядок расположения поля внутри формы.
            controlType: 'text',
            type: 'text',
            choices: null, // это для элемента <select></select>
          },
        ],
      },
    ];
    return {forms};
  }


}
